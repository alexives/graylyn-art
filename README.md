# Static Site Editor Template for GitLab Pages

## Making a change

1. Open the file you want in the [web ide](https://gitlab.com/-/ide/project/alexives/graylyn-art/tree/master/-/)
2. Click the "Commit" button
3. Click "Commit to master branch"
4. Click "Commit"

That'll put the changes in the repository and it'll automatically deploy a new version of the site with the change!

### [Edit items](https://gitlab.com/alexives/graylyn-art/-/edit/master/data/items.yml)
### [Edit the paypal link](https://gitlab.com/alexives/graylyn-art/-/edit/master/data/config.yml)
### [Edit the Homepage](https://gitlab.com/alexives/graylyn-art/-/blob/master/source/index.html.md)

## How to use the Static Site Editor feature

1. Open `https://graylyn.ives.mn` link in your browser.
1. Click on `Edit this page` button on the bottom of the website.
1. Use the Static Site Editor to make changes to the content and save the changes by creating a merge request.
1. Merge your merge request to master branch.
1. GitLab CI will automatically apply changes to your website.

## Local development

1. Clone this project.
1. Download dependencies: `bundle install`. If you see an error that bundler is missing, then try to run `gem install bundler`.
1. Run the project: `bundle exec middleman`.
1. Open http://localhost:4567 in your browser.

## Useful information

[GitLab Pages default domain names](https://docs.gitlab.com/ee/user/project/pages/getting_started_part_one.html#gitlab-pages-default-domain-names)

## Learning Middleman

* Visit [Middleman website](https://middlemanapp.com)
* Take a look at [the documentation](https://middlemanapp.com/basics/install)
* Explore [the source code](https://github.com/middleman/middleman)
